#Lib server
from flask import Flask, request, jsonify, abort
from flask_pymongo import PyMongo
# pip install geopy
from geopy.geocoders import Nominatim
import geopy.distance
# pip install requests
import requests
import json

#Lib IA
from sklearn.neighbors import KNeighborsClassifier
from sklearn import tree
from sklearn.datasets import load_iris
import mglearn
import matplotlib.pyplot as plt
from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import train_test_split
from sklearn import preprocessing


app = Flask(__name__)

app.config["MONGO_DBNAME"] = "easypath"
app.config["MONGO_URI"] = "mongodb://localhost:27017/easypath"

mongo = PyMongo(app)

#########################
###### PARTIE IA ########
#########################

def IA(typeroute, speed):
    #print(typeroute)
    #print(speed)
    # First Feature
    maxSpeed = []
    routeType = []
    vehicle = []

    #recuperation des donn�es de la BDD
    for data in mongo.db.vehicules.find({"Vitesse":speed}):

        maxSpeed.append(data.get("Vitesse").replace(".0", ""))
        # Second Feature
        routeType.append(getNumberTypeRoute(data.get("routeType")))
        # Label or target varible
        vehicle.append(data.get("Vehicule"))
   
    #creating labelEncoder
    le = preprocessing.LabelEncoder()
    # Converting string labels into numbers.
    routeType_encoded= [int(i) for i in routeType]
    maxSpeed_encoded = [int(i) for i in maxSpeed]

    features=list(zip(routeType_encoded,maxSpeed_encoded))
    label=le.fit_transform(vehicle)
      
    #OLD ALGORITHM
    #model = KNeighborsClassifier(n_neighbors=3)

    #NEW ALGORITHM
    model = tree.DecisionTreeClassifier()
    
    # Train the model using database EASYPATH
    model.fit(features,label)
    
    #Calcul de la pr�sition ( 0 < pourcentage < 1 )
    X_train, X_test, y_train, y_test = train_test_split(features, label, test_size=0.33, random_state=42)
    print(model.score(X_test,y_test))
    
    predicted= model.predict([[typeroute,speed.replace(".0", "")]])

    vehic = vehicle[predicted[0]]
    print(vehic)

    if(vehic == "Car"):
        return "en voiture"
    if(vehic == "Bike"):
        return "a velo"
    else:
        return "a pied"
    

@app.route('/ai', methods=['POST']) 
def comIA():
    if(request.get_json() is False):
      print("Error get json")
      abort(400)
    
    content = request.get_json()
    
    try:
        speed = content.get("Vitesse")
        longitude = content.get("Longitude")
        latitude = content.get("Latitude")
        typeroute = parseCoords(latitude, longitude)
        numberTypeRoute = getNumberTypeRoute(typeroute)
        predicted = IA(numberTypeRoute, speed)
        return predicted
    except Exception as e:
      print(e)
      return "a pied"
      abort(400)

#200 = highway, 0 = amenity, 1 = building, 2 = historic, 3 = landuse
#180 = cycleway, 40 = pedestrian, 30 = path
   
      
#Convertir le type de route en numero       
def getNumberTypeRoute(typeroute):
  with open("config_buildings.json") as f:
    data = json.loads(f.read())
  try:
      if data[typeroute]:
          return data[typeroute]
      else:
          return 6
  except Exception as e:
    print(e)
    abort(400)



##############################
####### PARTIE SERVEUR #######
##############################
    

#Recupere le type de rotue selon la position    
def parseCoords(latitude, Longitude):

      url = "http://nominatim.openstreetmap.org/reverse"
      lat = latitude
      lon = Longitude
      payload = {'format':'jsonv2', 'lat':lat, 'lon':lon}
      resp = requests.post(url, params=payload  )

      if resp.ok:
          info = json.loads(resp.text)
          category = info['category']
          type_device = info['type']
          #print(category)
          return category
          abort(400)
      else:
          print ("Error parse coord: (lat: %f,long: %f)" %(lat, long))
  

#Recupere les parkings de v�lo ou de voiture          
@app.route('/parks', methods=['POST'])    
def findParks():

      data = request.get_json()
      url = "http://nominatim.openstreetmap.org/search"
      lat1 = data["Lat1"]
      lat2 = data["Lat2"]
      lon1 = data["Lon1"]
      lon2 = data["Lon2"]
      latDest = data["destLat"]
      lonDest = data["destLon"]
      choix = data["choix"]
      viewbox = lon1+','+lat1+','+lon2+','+lat2
      if(choix=="1"):
          payload = {'format':'jsonv2', 'limit':20, 'q':'bicycle parking','bounded':1,'viewbox':viewbox}
      else:
          payload = {'format':'jsonv2', 'limit':20, 'q':'parking','bounded':1,'viewbox':viewbox}
      resp = requests.post(url, params=payload  )
      if resp.ok:
          info = json.loads(resp.text)
          parkLat = None
          parkLon = None
          name = None
          data = []
          for result in info:
            dist = getDistance(result.get("lat"),result.get("lon"),latDest,lonDest)
            parkLat = result.get("lat")
            parkLon = result.get("lon")
            name = result.get("display_name")
            data.append(name)
            data.append(str(int(dist*1000)))
            data.append(parkLat)
            data.append(parkLon)
        
          parkings = ';'.join(data)
          if bool(parkings and parkings.strip()):
              print("Parking trouv�")
              return parkings
              abort(400)
          else:
              print("Aucun parking trouv�")
              return "null"
              abort(400)
      else:
          print ("Error parse viewbox")
          return None

#calcul la distance entre deux points 
def getDistance(lat1,lon1,lat2,lon2):
    
    coords_1 = (lat1, lon1)
    coords_2 = (lat2, lon2)
    return geopy.distance.distance(coords_1, coords_2).km


def addJson():

  data = request.get_json()
  vehicule = data['Vehicule']
  latitude = data['Latitude']
  longitude = data['Longitude']
  vitesse = data['Vitesse']


  return jsonify({'Vehicule' : vehicule, 'Latitude' : latitude, 'Longitude' : longitude, 'Vitesse' : vitesse, 'category' : parseCoords(latitude, longitude)})


@app.route('/receive', methods=['POST'])
def main():

	#Enregistre les donn�es dans la BDD
    mycol = mongo.db.vehicules
    data = request.get_json()
    latitude = data['Latitude']
    longitude = data['Longitude']
    if(latitude !="0.0" and longitude !="0.0" and data["Vitesse"]!="0.0"): 
        data['routeType'] = parseCoords(latitude,longitude)
        mycol.insert_one(data)
        print("Donn�es enregistr�es")
    else:
        print("Donn�es non enregistr�es")
    abort(500)
    

if __name__ == '__main__':
    app.run(host='0.0.0.0')
