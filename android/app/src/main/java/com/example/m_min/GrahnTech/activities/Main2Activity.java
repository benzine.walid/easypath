package com.example.m_min.GrahnTech.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.m_min.GrahnTech.async.PostTask;
import com.example.m_min.GrahnTech.R;
import com.example.m_min.GrahnTech.draw.CircleOverlay;
import com.example.m_min.GrahnTech.utils.Settings;
import com.example.m_min.GrahnTech.webSocket.MessageListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.osmdroid.bonuspack.routing.MapQuestRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polyline;

public class Main2Activity extends AppCompatActivity {

    private Handler handler;
    private FusedLocationProviderClient mFusedLocationClient;
    private int MY_PERMISSIONS_REQUEST_FINE_LOCATION;
    private int MY_PERMISSIONS_REQUEST_COARSE_LOCATION;
    private AppLocationManager app;
    private JSONGenerator jsong;
    private String ID,destination;
    private MessageListener messList;
    private MapView map ;
    Location destLOCATION;
    private static final int REQUEST_CODE_PERMISSION = 1;
    String mPermission = Manifest.permission.ACCESS_FINE_LOCATION;
    double rayon=1000;
    String latPark,lonPark;
    String[] choix = {"A PIED","A VELO", "EN VOITURE"};
    Button select,send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //MISE EN PLACE DES AUTORISATIONS
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(ctx));
        messList = new MessageListener(this);
        if(Build.VERSION.SDK_INT>= 23) {
            if (checkSelfPermission(mPermission) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(Main2Activity.this,
                        new String[]{mPermission,
                        },
                        REQUEST_CODE_PERMISSION);
                return;
            }
        }

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_FINE_LOCATION);

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_COARSE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
        //Demande des autorisations

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_FINE_LOCATION);

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_COARSE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }

        // APPLICATION ET RECUPERATION DES LAYOUTS
        setContentView(R.layout.activity_main2);
        send = findViewById(R.id.predict);
        select = findViewById(R.id.select);
        Button search = findViewById(R.id.search);
        Button changeActivity = findViewById(R.id.datasend);
        EditText rayonText = findViewById(R.id.rayon);
        TextView dataText = findViewById(R.id.data);
        EditText dest = findViewById(R.id.dest);

        //DECLARATION DES OBJETS IMPORTANTS
        app = new AppLocationManager(ctx, dataText);
        jsong = new JSONGenerator();
        ID = UUID.randomUUID().toString();

        // Création de la carte et des marquers
        map = findViewById(R.id.map);
        map.setClickable(true);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        map.getController().setZoom(16);

        Marker destMarker = new Marker(map);
        destMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        destMarker.setIcon(getResources().getDrawable(R.drawable.destination));
        destMarker.setTitle("Ma destination");

        Marker position = new Marker(map);
        position.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        position.setIcon(getResources().getDrawable(R.drawable.position));
        position.setTitle("Ma position");

        Marker markPark = new Marker(map);
        markPark.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        markPark.setIcon(getResources().getDrawable(R.drawable.parking));
        markPark.setTitle("Parking");

        CircleOverlay circleDraw = new CircleOverlay();

        if(Double.parseDouble(app.getLatitude()) != 0 && Double.parseDouble(app.getLongitude()) != 0) {
            //Centrer la map sur la position GPS utilisateur
            map.getController().setCenter(new GeoPoint(Double.parseDouble(app.getLatitude()), Double.parseDouble(app.getLongitude())));
            position.setPosition(new GeoPoint(Double.parseDouble(app.getLatitude()), Double.parseDouble(app.getLongitude())));
            map.getOverlays().add(position);
        }
        else{
            //CENTRER SUR PARIS
            map.getController().setCenter(new GeoPoint(48.854530, 2.347749));
        }



        handler = new Handler();
        send.setTag(1);

        //Prediction si le bouton PREDICT est pressé
        send.setOnClickListener((view) -> {

            if((Integer) view.getTag() == 1){
                dataText.setText("Calcul...");
                send.setText("Stop");
                send.setTag(0);
                handler.postDelayed( new Runnable() {
                                         public void run() {
                                             try {
                                                 sendDataFct(dataText);
                                             } catch (IOException e) {
                                                 e.printStackTrace();
                                             } catch (URISyntaxException e) {
                                                 e.printStackTrace();
                                             }
                                             handler.postDelayed(this, 1000);
                                         }
                                     }
                        , 100);
            } else {
                send.setTag(1);
                send.setText("Predict");
                handler.removeCallbacksAndMessages(null);
            }
        });

        changeActivity.setOnClickListener((view) -> {
            Intent myIntent = new Intent(this, MainActivity.class);
            startActivity(myIntent);
        });

        select.setTag(0);
        //TAG 0 = A PIED, TAG 1 = EN VELO, TAG 2 = EN VOITURE
        //AFFICHAGE DE LA LISTE DES CHOIX UTILISATEURS
        select.setOnClickListener((view -> {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Vous êtes :");
                    builder.setItems(choix, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch(which){
                                // UTILISATEUR A PIED
                                case 0: dataText.setText("Vous êtes a pied");
                                        select.setTag(0);
                                        break;
                                        //UTILISATEUR A VELO
                                case 1: dataText.setText("Vous êtes a velo");
                                        select.setTag(1);
                                        break;
                                        //UTILISATEUR EN VOITURE
                                case 2: dataText.setText("Vous êtes en voiture");
                                        select.setTag(2);
                                        break;
                            }
                        }
                    });
                    //Affiche la box de dialog
                    AlertDialog dialog2 = builder.create();
                    dialog2.show();
        }));

        search.setOnClickListener(view -> {
            // RECHERCHE DE PARKINGS ET ITINERAIRES SELON LE CAS UTILISATEUR
            destination = dest.getText().toString();
            if(destination != null && !destination.isEmpty()){
                destLOCATION = app.getLocationFromAddress(this,destination);
                if(destLOCATION != null) {

                    //vide la carte
                    map.getOverlays().clear();

                    if(Double.parseDouble(app.getLatitude()) != 0 && Double.parseDouble(app.getLongitude()) != 0) {
                        position.setPosition(new GeoPoint(Double.parseDouble(app.getLatitude()), Double.parseDouble(app.getLongitude())));
                        //positionne le marquer position
                        map.getOverlays().add(position);
                    }

                    destMarker.setPosition(new GeoPoint(destLOCATION.getLatitude(), destLOCATION.getLongitude()));

                    if( !TextUtils.isEmpty(rayonText.getText().toString()) ) {
                        //récupere le rayon choisi  [1km par défaut]
                        rayon = Double.parseDouble(rayonText.getText().toString());
                    }
                    // affichage du cercle selon le rayon
                    circleDraw.createCircleOverlay(ctx, destLOCATION.getLatitude(), destLOCATION.getLongitude(), (float) (rayon));

                    double circle[] = getCircle(destLOCATION.getLatitude(), destLOCATION.getLongitude(), rayon);
                    //Positionne le marquer de destination
                    map.getOverlays().add(destMarker);

                    //centre la carte sur le point de destination
                    map.getController().setCenter(new GeoPoint(destLOCATION.getLatitude(), destLOCATION.getLongitude()));

                    //Si le moyen de transport est diffèrent que A PIED
                    if ( Integer.parseInt(select.getTag().toString()) != 0) {
                        try {
                            //apelle la methode recherche de parkings du serveur
                            String[] parks = getParks(circle, dataText, destLOCATION);

                            if (parks != null) {
                                //recupere la liste des parkings avec la distance
                                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                                builder.setTitle("Choisissez un parking");
                                String[] parkings = new String[parks.length / 4];
                                for (int i = 0; i < parks.length; i = i + 4) {
                                    parkings[i / 4] = parks[0] + " [" + parks[i + 1] + "mètres]";
                                }

                                builder.setItems(parkings, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        //recuperation du choix de parking de l'utilisateur
                                        latPark = parks[which * 4 + 2];
                                        lonPark = parks[which * 4 + 3];

                                        //positionne le marquer parking sur la carte
                                        markPark.setPosition(new GeoPoint(Double.parseDouble(latPark), Double.parseDouble(lonPark)));
                                        map.getOverlays().add(markPark);
                                        map.getOverlays().add(circleDraw);

                                        //calcul  les itinéraires
                                        ArrayList<GeoPoint> waypoints = new ArrayList<GeoPoint>();
                                        ArrayList<GeoPoint> waypoints2 = new ArrayList<GeoPoint>();
                                        GeoPoint startPoint = new GeoPoint(Double.parseDouble(app.getLatitude()), Double.parseDouble(app.getLongitude()));
                                        waypoints.add(startPoint);
                                        GeoPoint parkPoint = new GeoPoint(Double.parseDouble(latPark), Double.parseDouble(lonPark));
                                        waypoints.add(parkPoint);
                                        waypoints2.add(parkPoint);
                                        GeoPoint endPoint = new GeoPoint(destLOCATION.getLatitude(), destLOCATION.getLongitude());
                                        waypoints2.add(endPoint);

                                        RoadManager roadManager = new MapQuestRoadManager("R9OJ2bfQBM44wE0FYUuncEFoPl7SXXUx");
                                        RoadManager roadManager2 = roadManager;

                                        if(Integer.parseInt(select.getTag().toString()) == 1) {
                                            //itinéraire à velo
                                            roadManager.addRequestOption("routeType=bicycle");
                                        }
                                        else {
                                            //itinéraire en voiture
                                            roadManager.addRequestOption("routeType=fastest");
                                        }
                                        //le reste de l'itinéraire a pied
                                        roadManager2.addRequestOption("routeType=pedestrian");

                                        try {
                                            Road road = roadManager.getRoad(waypoints);
                                            Road road2 = roadManager2.getRoad(waypoints2);

                                            if (road.mStatus != Road.STATUS_OK && road2.mStatus != Road.STATUS_OK) {
                                                Toast.makeText(getBaseContext(), "Aucun chemin trouvé", Toast.LENGTH_SHORT).show();
                                            }
                                            else {
                                                //dessin des itinéraires
                                                Polyline roadOverlay = RoadManager.buildRoadOverlay(road, Color.RED, 9);
                                                Polyline roadOverlay2 = RoadManager.buildRoadOverlay(road2, Color.BLUE, 4);
                                                map.getOverlays().add(roadOverlay);
                                                map.getOverlays().add(roadOverlay2);
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Toast.makeText(getBaseContext(), "Aucun chemin trouvé", Toast.LENGTH_SHORT).show();
                                        }
                                        //actualisation de la map
                                        map.invalidate();
                                    }
                                });
                                AlertDialog dialog = builder.create();
                                dialog.show();

                            } else {
                                Toast.makeText(getBaseContext(), "Aucune parking trouvé pour le rayon donné", Toast.LENGTH_SHORT).show();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                    }
                    else {

                        // ITINERAIRE A PIED PAR DEFAUT
                        ArrayList<GeoPoint> waypoints = new ArrayList<GeoPoint>();
                        GeoPoint startPoint = new GeoPoint(Double.parseDouble(app.getLatitude()), Double.parseDouble(app.getLongitude()));
                        waypoints.add(startPoint);
                        GeoPoint endPoint = new GeoPoint(destLOCATION.getLatitude(), destLOCATION.getLongitude());
                        waypoints.add(endPoint);

                        RoadManager roadManager = new MapQuestRoadManager("R9OJ2bfQBM44wE0FYUuncEFoPl7SXXUx");
                        roadManager.addRequestOption("routeType=pedestrian");

                        try {
                            Road road = roadManager.getRoad(waypoints);

                            if (road.mStatus != Road.STATUS_OK) {
                                Toast.makeText(getBaseContext(), "Aucun chemin trouvé", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Polyline roadOverlay = RoadManager.buildRoadOverlay(road, Color.RED, 8);
                                dataText.setText("vous êtes a pied");
                                map.getOverlays().add(roadOverlay);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getBaseContext(), "Aucun chemin trouvé", Toast.LENGTH_SHORT).show();
                        }
                        map.invalidate();
                    }
                    dest.setText("");
                }
                else{
                    Toast.makeText(getBaseContext(),"Aucune adresse trouvée",Toast.LENGTH_LONG).show();
                }
            }
            else{
                Toast.makeText(getBaseContext(),"Adresse non valide",Toast.LENGTH_SHORT).show();
            }
        });


    }

    @SuppressLint("MissingPermission")
    public void sendDataFct(TextView dataText) throws IOException, URISyntaxException {
        Map<String, String> data;
        data = jsong.getJSONPredict(app);
        AsyncTask task = new PostTask(Settings.loadSettings(this), "ai", dataText);
            try {
                send.setTag(0);
               task.execute(new Map[]{data});
            } finally{
                try {
                    String result = (String)task.get();
                    if(result==null || result.equals("null")){
                        dataText.setText("Calcul...");
                    }
                    else {
                        dataText.setText("vous êtes " + result);
                        if(result.equals("a velo")){
                            select.setTag(1);
                        }
                        if(result.equals("en voiture")){
                            select.setTag(2);
                        }
                        if(result.equals("a velo")){
                            select.setTag(1);
                        }
                        if(result.equals("a pied")){
                            select.setTag(0);
                        }
                    }
                    System.out.println(result);

                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        //DESSINE LE CERCLE EN FONCTION DU RAYON ET DE LA POSITION
        public double[] getCircle(double lat,double lon,double rayon){
        double southLat = lat - rayon/111110;
        double northhLat = lat + rayon/111110;
        double westLong = lon - rayon/111110;
        double easthLong = lon + rayon/111110;
        double circle[] = {southLat,northhLat,westLong,easthLong};
        return circle;
        }


        //RECUPERE LES PARKINGS
    public String[] getParks(double circle[], TextView dataText, Location destination) throws IOException, URISyntaxException{

        Map<String, String> data = new HashMap<String, String>();
        data.put("Lat1", String.valueOf(circle[0]));
        data.put("Lat2", String.valueOf(circle[1]));
        data.put("Lon1", String.valueOf(circle[2]));
        data.put("Lon2", String.valueOf(circle[3]));
        data.put("destLat", String.valueOf(destination.getLatitude()));
        data.put("destLon", String.valueOf(destination.getLongitude()));
        data.put("choix", select.getTag().toString());
        AsyncTask task2 = new PostTask(Settings.loadSettings(this), "parks", dataText);

        try {
            task2.execute(new Map[]{data});
        } finally{
            try {
                String result = task2.get().toString();
                if(result.equals("null")){
                    return null;
                }
                else{
                String[] parks= result.split(";");
                for (int i=0;i<parks.length;i=i+4){
                    String[] temp = parks[i].split(",");
                    parks[i] = temp[0] + ","+temp[1];
                }
                return parks;}
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    }





