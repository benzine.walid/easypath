package com.example.m_min.GrahnTech.webSocket;

import android.content.Context;

import com.example.m_min.GrahnTech.utils.Notification;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class MessageListener extends WebSocketListener {

    private Context context;

    public MessageListener(Context context) {
        this.context = context;
    }

    public void onOpen(WebSocket webSocket, Response response) {
        System.out.println("open websocket");

    }

    public void onMessage(WebSocket webSocket, String text) {
        Notification notification = new Notification(context);
        notification.createNotificationChannel();
        notification.openNotification(text);
    }

    public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(code, reason);
    }

    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        t.printStackTrace();
    }
}