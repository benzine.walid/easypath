package com.example.m_min.GrahnTech.draw;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.Projection;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.MapView;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

public class CircleOverlay extends Overlay {

    Context context;
    double mLat;
    double mLon;
    float mRadius;
    Paint innerCirclePaint;

    public CircleOverlay() {
            super();
        innerCirclePaint = new Paint();
        // La couleur du cercle
        innerCirclePaint.setColor(Color.RED);

        // La transparence du cercle 0 - transparent; 100 - plus foncé
        innerCirclePaint.setAlpha(50);
        innerCirclePaint.setAntiAlias(true);

        innerCirclePaint.setStyle(Paint.Style.FILL);
    }


    public void createCircleOverlay(Context _context, double _lat, double _lon, float radius ) {
        context = _context;
        mLat = _lat;
        mLon = _lon;
        mRadius = radius;
    }


    public void draw(Canvas canvas, MapView mapView, boolean shadow) {
        //super.draw(canvas, mapView, shadow);

        if(shadow) return; // Ignore the shadow layer

        Projection projection = mapView.getProjection();

        Point pt = new Point();

        GeoPoint geo = new GeoPoint((int) (mLat *1e6), (int)(mLon * 1e6));

        projection.toPixels(geo ,pt);
        double circleRadius = projection.metersToEquatorPixels(mRadius) * (1/ Math.cos((float) Math.toRadians(mLat)));

        canvas.drawCircle((float)pt.x, (float)pt.y, (float)circleRadius, innerCirclePaint);
    }
}
