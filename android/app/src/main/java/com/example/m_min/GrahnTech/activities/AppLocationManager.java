package com.example.m_min.GrahnTech.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.TextView;

import org.osmdroid.views.MapView;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

public class AppLocationManager implements LocationListener {


    private LocationManager locationManager;
    private double latitude=0;
    private double longitude=0;
    private float speed;
    private Criteria criteria;
    private String provider;
    private float maxSpeed = 0;
    private double maxLat;
    private double maxLon;
    private String destination;
    private Location lastKnownLoc;
    private long lastChange;
    private TextView dataText;

    public AppLocationManager(Context context, TextView tv) {
        locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        provider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000,
                0, this);
        setMostRecentLocation(locationManager.getLastKnownLocation(provider));

        Location location = null;
        if (locationManager!= null) {
            location = locationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
        }

        lastChange = Calendar.getInstance().getTimeInMillis();

        dataText = tv;
    }

    private void setMostRecentLocation(Location lastKnownLocation) {
        this.lastKnownLoc =  lastKnownLocation;
    }

    public String getLatitude() {
        return String.valueOf(latitude);
    }

    public String getLongitude() {
        return String.valueOf(longitude);
    }

    public String getSpeed(){
        return String.valueOf(speed);
    }

    public String getMaxLat() {

        return String.valueOf(maxLat);
    }

    public String getMaxLon() {

        return String.valueOf(maxLon);
    }

    public String getMaxSpeed(){
        return String.valueOf(maxSpeed);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * android.location.LocationListener#onLocationChanged(android.location.
     * Location)
     */
    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {

            float newSpeed = (float) (location.getSpeed() * 3.6);
            newSpeed = (float) Math.floor(newSpeed);
            if(newSpeed > this.maxSpeed){
                maxSpeed = newSpeed;
                maxLat = location.getLatitude();
                maxLon = location.getLongitude();
            }

            this.lastKnownLoc = location;
            this.latitude = location.getLatitude();
            this.longitude = location.getLongitude();
            this.speed = newSpeed;

            //dataText.setText(String.valueOf(speed) + " || " + String.valueOf(maxSpeed));

        }

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * android.location.LocationListener#onProviderDisabled(java.lang.String)
     */
    @Override
    public void onProviderDisabled(String arg0) {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * android.location.LocationListener#onProviderEnabled(java.lang.String)
     */
    @Override
    public void onProviderEnabled(String arg0) {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     *
     * @see android.location.LocationListener#onStatusChanged(java.lang.String,
     * int, android.os.Bundle)
     */
    @Override
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
        // TODO Auto-generated method stub

    }

    public Location getLocationFromAddress(Context context,String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        Location loc = new Location("");

        try {
            // May throw an IOException
            if(strAddress!=null && !strAddress.isEmpty()) {
                address = coder.getFromLocationName(strAddress, 5);
            }
            else{
                return null;
            }

            if (address.size() != 0 ) {
                Address location = address.get(0);
                loc.setLatitude(location.getLatitude());
                loc.setLongitude(location.getLongitude());
            }
            else{
                return null;
            }


        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return loc;
    }

}
