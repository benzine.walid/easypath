package com.example.m_min.GrahnTech.utils;

import android.content.Context;

import com.example.m_min.GrahnTech.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Settings {

    private final String url;

    public Settings(String url) {
        this.url = url;
    }

    public static Settings loadSettings(Context context) throws IOException {
        InputStream rawResource = context.getResources().openRawResource(R.raw.config);
        Properties properties = new Properties();
        properties.load(rawResource);
        return new Settings(properties.getProperty("url"));
    }

    public String getUrl() {
        return url;
    }

}
