package com.example.m_min.GrahnTech.utils;

import java.io.IOException;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Post {

    public static final MediaType JSON
            = MediaType.get("application/json; charset=utf-8");


    public static String post(String host, int port, String path, Map<String, String> data) throws IOException {
        System.out.println(host + " " + port);
        OkHttpClient client = new OkHttpClient();

        String json = data.entrySet().stream().map(d -> "\""+d.getKey()+"\":\""+d.getValue()+"\"").reduce((s, s2) -> s+","+s2).map(s -> "{"+s+"}").get();

        System.out.println(json);
        RequestBody body = RequestBody.create(JSON, json);

        HttpUrl.Builder b = new HttpUrl.Builder()
                .scheme("http")
                .host(host)
                .port(port)
                .addPathSegment(path);

//        for(Map.Entry<String,String> d : data.entrySet() ){
//            b.addQueryParameter(d.getKey(),d.getValue());
//        }
//
        HttpUrl httpUrl = b.build();


        Request request = new Request.Builder()
                .addHeader("charset", "utf-8")
                .addHeader("Connection", "close")
                .url(httpUrl)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        String result = response.body().string();

        response.close();
        return result;

    }
}
